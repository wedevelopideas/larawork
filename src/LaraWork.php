<?php

declare(strict_types=1);

namespace SandraGranath\LaraWork;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Schema;

class LaraWork
{
    /**
     * Name of the database upon which the seed will be executed.
     *
     * @var string
     */
    protected $databaseName;

    /**
     * New line character for seed files.
     * Double quotes are mandatory!
     *
     * @var string
     */
    private $newLineCharacter = PHP_EOL;

    /**
     * Desired indent for the code.
     * For tabulator use \t
     * Double quotes are mandatory!
     *
     * @var string
     */
    private $indentCharacter = '    ';

    /**
     * @var Filesystem
     */
    private $files;

    /**
     * LaraWork constructor.
     * @param Filesystem|null $filesystem
     */
    public function __construct(Filesystem $filesystem = null)
    {
        $this->files = $filesystem ?: new Filesystem();
    }

    /**
     * Generates a seed class name.
     *
     * @param string $table
     * @return string
     */
    public function generateClassName(string $table): string
    {
        $tableString = '';
        $tableName = explode('_', $table);

        foreach ($tableName as $tableNameExploded) {
            $tableString .= ucfirst($tableNameExploded);
        }

        return ucfirst($tableString).'TableSeeder';
    }

    /**
     * Generates a seed file.
     *
     * @param string $table
     * @param null $database
     * @return bool
     */
    public function generateSeed(string $table, $database = null): bool
    {
        if (! $database) {
            $database = config('database.default');
        }

        $this->databaseName = $database;

        if (! $this->hasTable($table)) {
            throw new TableNotFoundException("Table $table was not found.");
        }

        $data = $this->getData($table);

        $dataArray = $this->repackSeedData($data);

        $className = $this->generateClassName($table);

        $stub = $this->readStubFile($this->getStubPath().'/seeder.stub');

        $seedPath = $this->getSeedPath();

        $seedsPath = $this->getPath($className, $seedPath);

        $seedContent = $this->populateStub(
            $className,
            $stub,
            $table,
            $dataArray
        );

        $this->files->put($seedsPath, $seedContent);

        return $this->updateDatabaseSeederRunMethod($className) !== false;
    }

    /**
     * Get all data.
     *
     * @param  string $table
     * @return Collection
     */
    public function getData($table): Collection
    {
        $result = DB::connection($this->databaseName)->table($table);

        return $result->get();
    }

    /**
     * Create the full path name to the seed file.
     *
     * @param  string  $name
     * @param  string  $path
     * @return string
     */
    public function getPath(string $name, string $path): string
    {
        return $path.'/'.$name.'.php';
    }

    /**
     * Get a seed folder path.
     *
     * @return string
     */
    public function getSeedPath(): string
    {
        return base_path().'/database/seeds';
    }

    /**
     * Get the path to the stub file.
     *
     * @return string
     */
    public function getStubPath(): string
    {
        return __DIR__.DIRECTORY_SEPARATOR.'Stubs';
    }

    /**
     * Checks if a database table exists.
     *
     * @param string $table
     * @return bool
     */
    public function hasTable($table): bool
    {
        return Schema::connection($this->databaseName)->hasTable($table);
    }

    /**
     * Populate the place-holders in the seed stub.
     *
     * @param string $className
     * @param string $stub
     * @param string $table
     * @param array $dataArray
     * @return mixed|string
     */
    public function populateStub(string $className, string $stub, string $table, array $dataArray)
    {
        $chunkSize = 500;

        $inserts = '';
        $chunks = array_chunk($dataArray, $chunkSize);
        foreach ($chunks as $chunk) {
            $this->addNewLines($inserts);
            $this->addIndent($inserts, 2);
            $inserts .= sprintf(
                "DB::table('%s')->insert(%s);",
                $table,
                $this->prettifyArray($chunk, true)
            );
        }
        $stub = str_replace('{{class}}', $className, $stub);

        $stub = str_replace('{{insert_statements}}', $inserts, $stub);

        return $stub;
    }

    /**
     * Read stub file.
     *
     * @param $file
     * @return string
     */
    public function readStubFile($file): string
    {
        $buffer = file($file, FILE_IGNORE_NEW_LINES);

        return implode(PHP_EOL, $buffer);
    }

    /**
     * Repacks data read from the database.
     *
     * @param  array|object $data
     * @return array
     */
    public function repackSeedData($data): array
    {
        if (! is_array($data)) {
            $data = $data->toArray();
        }
        $dataArray = [];
        if (! empty($data)) {
            foreach ($data as $row) {
                $rowArray = [];
                foreach ($row as $columnName => $columnValue) {
                    $rowArray[$columnName] = $columnValue;
                }
                $dataArray[] = $rowArray;
            }
        }

        return $dataArray;
    }

    /**
     * Adds new lines to the passed content variable reference.
     *
     * @param string    $content
     * @param int       $numberOfLines
     */
    private function addNewLines(&$content, $numberOfLines = 1): void
    {
        while ($numberOfLines > 0) {
            $content .= $this->newLineCharacter;
            $numberOfLines--;
        }
    }

    /**
     * Adds indentation to the passed content reference.
     *
     * @param string    $content
     * @param int       $numberOfIndents
     */
    private function addIndent(&$content, $numberOfIndents = 1): void
    {
        while ($numberOfIndents > 0) {
            $content .= $this->indentCharacter;
            $numberOfIndents--;
        }
    }

    /**
     * Prettify a var_export of an array.
     *
     * @param  array  $array
     * @return string
     */
    protected function prettifyArray($array, $indexed = true): string
    {
        $content = ($indexed)
            ? var_export($array, true)
            : preg_replace("/[0-9]+ \=\>/i", '', var_export($array, true));
        $lines = explode("\n", $content);
        $inString = false;
        $tabCount = 3;
        for ($i = 1; $i < count($lines); $i++) {
            $lines[$i] = ltrim($lines[$i]);
            //Check for closing bracket
            if (strpos($lines[$i], ')') !== false) {
                $tabCount--;
            }
            //Insert tab count
            if ($inString === false) {
                for ($j = 0; $j < $tabCount; $j++) {
                    $lines[$i] = substr_replace($lines[$i], $this->indentCharacter, 0, 0);
                }
            }
            for ($j = 0; $j < strlen($lines[$i]); $j++) {
                //skip character right after an escape \
                if ($lines[$i][$j] == '\\') {
                    $j++;
                }
                //check string open/end
                elseif ($lines[$i][$j] == '\'') {
                    $inString = ! $inString;
                }
            }
            //check for openning bracket
            if (strpos($lines[$i], '(') !== false) {
                $tabCount++;
            }
        }
        $content = implode("\n", $lines);

        return $content;
    }

    /**
     * Updates the DatabaseSeeder file's run method.
     *
     * @param  string $className
     * @return bool
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function updateDatabaseSeederRunMethod($className): bool
    {
        $databaseSeederPath = base_path().config('iseed::config.path').'/DatabaseSeeder.php';
        $content = $this->files->get($databaseSeederPath);
        if (strpos($content, "\$this->call({$className}::class)") === false) {
            if (
                strpos($content, '#iseed_start') &&
                strpos($content, '#iseed_end') &&
                strpos($content, '#iseed_start') < strpos($content, '#iseed_end')
            ) {
                $content = preg_replace("/(\#iseed_start.+?)(\#iseed_end)/us", "$1\$this->call({$className}::class);{$this->newLineCharacter}{$this->indentCharacter}{$this->indentCharacter}$2", $content);
            } else {
                $content = preg_replace("/(run\(\).+?)}/us", "$1{$this->indentCharacter}\$this->call({$className}::class);{$this->newLineCharacter}{$this->indentCharacter}}", $content);
            }
        }

        return $this->files->put($databaseSeederPath, $content) !== false;
    }
}
