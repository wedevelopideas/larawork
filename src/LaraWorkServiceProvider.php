<?php

declare(strict_types=1);

namespace SandraGranath\LaraWork;

use Illuminate\Support\ServiceProvider;

class LaraWorkServiceProvider extends ServiceProvider
{
    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind('larawork', LaraWork::class);
        $this->app->bind('command.work', Console\WorkCommand::class);

        $this->commands('command.work');
    }
}
