<?php

declare(strict_types=1);

namespace SandraGranath\LaraWork\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Symfony\Component\Console\Input\InputOption;
use SandraGranath\LaraWork\TableNotFoundException;
use Symfony\Component\Console\Input\InputArgument;

class WorkCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'work';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Just a test at the moment.';

    /**
     * WorkCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $tables = explode(',', $this->argument('tables'));

        foreach ($tables as $table) {
            $table = trim($table);

            list($fileName, $className) = $this->generateFileName($table);

            if (! File::exists($fileName)) {
                $this->printResult(
                    app('larawork')->generateSeed($table, $this->option('database')), $table
                );
                continue;
            }

            if ($this->confirm('File '.$className.' already exist. Do you wish to override it? [yes|no]')) {
                $this->printResult(
                    app('larawork')->generateSeed($table, $this->option('database')), $table
                );
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments(): array
    {
        return [
            ['tables', InputArgument::OPTIONAL, 'comma separated string of table names'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions(): array
    {
        return [
            ['database', null, InputOption::VALUE_OPTIONAL, 'database connection', Config::get('database.default')],
        ];
    }

    /**
     * Generates file name.
     *
     * @param string $table
     * @return array
     */
    protected function generateFileName(string $table): array
    {
        if (! Schema::connection($this->option('database') ? $this->option('database') : Config('database.default')->hasTable($table))) {
            throw new TableNotFoundException("Table $table was not found");
        }

        $className = app('larawork')->generateClassName($table);
        $seedPath = '/database/seeds';

        return [$seedPath.'/'.$className.'.php', $className.'.php'];
    }

    /**
     * Provide user feedback, based on success or not.
     *
     * @param  bool $successful
     * @param  string $table
     * @return void
     */
    protected function printResult($successful, $table): void
    {
        if ($successful) {
            $this->info("Created a seed file from table {$table}");

            return;
        }
        $this->error("Could not create seed file from table {$table}");
    }
}
