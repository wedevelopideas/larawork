<?php

declare(strict_types=1);

namespace SandraGranath\LaraWork;

class TableNotFoundException extends \RuntimeException
{
}
